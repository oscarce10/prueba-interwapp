# Prueba Interwapp

## About the project
Technical assessment presented to [Interwapp](https://www.interwap.co/) giving solutions to the following items:
<object data="https://oscarce10-github-projects.s3.amazonaws.com/prueba-interwap/PruebaTecnica.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="http://yoursite.com/the.pdf">
        <p>This browser <b>does not support PDFs</b>. Please download the PDF to view it: <a href="https://oscarce10-github-projects.s3.amazonaws.com/prueba-interwap/PruebaTecnica.pdf">Download PDF</a>.</p>
    </embed>
</object>

## Project description

- Language: PHP 7.4
- Framework: Laravel
- Database: MySQL
- Containers: Docker

## Running the project
To run the project, you need to install Docker and Docker Compose.  

Then you need to set up the environment variables by copying the `.env.example` file to the root directory of the project in a `.env` file.

Then you need to run the following commands:

```shell
docker run --rm -v $(pwd):/opt -w /opt laravelsail/php74-composer:latest composer install
```

```shell
./vendor/bin/sail up -d
```

```shell
./vendor/bin/sail migrate:fresh
```
